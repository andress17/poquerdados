package application;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * <h2>Joc</h2>
 * 
 * Classe on s'executa tot el joc.
 * @author Jonathan Raya Rios
 * @since 10-03-19
 */

public class ControladorJuego implements Initializable {
	
	Jugador jugador1;
	Jugador jugador2;
	
	@FXML
	private ImageView dadoTuyo5;
	@FXML
    private ImageView dadoTuyo4;
	@FXML
	private ImageView dadoTuyo3;
	@FXML
	private ImageView dadoTuyo2;
	@FXML
	private ImageView dadoTuyo1;
	@FXML
	private ImageView dadoRival5;
	@FXML
	private ImageView dadoRival4;
	@FXML
	private ImageView dadoRival3;
	@FXML
	private ImageView dadoRival2;
    @FXML
    private ImageView dadoRival1;
	@FXML
	private Label labelApuesta;
	@FXML
	private Button btnSubirApuesta;
	@FXML
	private Label labelDineroRival;
	@FXML
	private Button btnDado2;
	@FXML
	private Button btnDado3;
	@FXML
	private Button btnDado1;
	@FXML
	private Button btnRDado1;
	@FXML
	private Button btnRDado3;
	@FXML
	private Button btnRDado2;
	@FXML
	private Button btnDado4;
	@FXML
	private Button btnRDado5;
	@FXML
	private Button btnDado5;
	@FXML
	private Button btnRDado4;
	@FXML
	private Pane panelOpaco;
	@FXML
	private Button btnApostar;
	@FXML
	private Button btnBajarApuesta;
	@FXML
	private Label labelDinero;
	@FXML
	private ImageView imgDado2;
	@FXML
	private ImageView imgDado1;
	@FXML
	private ImageView imgDado4;
	@FXML
	private ImageView imgDado3;
	@FXML
	private ImageView imgDado5;
	@FXML
	private Pane panelApuesta;
	@FXML
	private Button btnTirarJugador1;
	@FXML
	private Button btnRecogerDadosJ1;
	@FXML
	private Button btnFinalizarJ1;
	@FXML
	private Button btnTirarJugador2;
	@FXML
	private Button btnRecogerDadosJ2;
	@FXML
	private Button btnFinalizarJ2;
    @FXML
    private Pane panelGanador;
    @FXML
    private ImageView imgGanador;
    @FXML
    private ImageView imgRondaGan;
	
	int[] valorDadosJ1 = new int[5];
	int[] valorDadosJ2 = new int[5];
	int dineroInicial;
    ImageView[] dadosSobreMesa = {imgDado1,imgDado2,imgDado3,imgDado4,imgDado5};
    ImageView[] dadosRival = {dadoRival1, dadoRival2, dadoRival3, dadoRival4, dadoRival5};
    ImageView[] tusDados = {dadoTuyo1, dadoTuyo2, dadoTuyo3, dadoTuyo4, dadoTuyo5};    
	Random random = new Random();
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) 
	{						   	
		juego();
	}
	
	/**
	 * Crea els dos objectes jugadors i els extableix els diners inicials.
	 * @see Jugador
	 */
	
	private void juego() 
	{
		labelDinero.setText((Integer.toString(ControladorInicial.dineroJugador1)));
    	labelDineroRival.setText((Integer.toString(ControladorInicial.dineroJugador1)));
    	dineroInicial = Integer.parseInt(labelDinero.getText());
    	btnBajarApuesta.setDisable(true);
    	jugador1 = new Jugador(Integer.parseInt(labelDinero.getText()));
    	jugador2 = new Jugador(Integer.parseInt(labelDinero.getText()));
    	inciarImagenes();
	}
	
	/**
	 * Metode per inicialitzar les diferents arrays d'imatges del programa.
	 * @see Jugador
	 */
	
	private void inciarImagenes() 
	{
		dadosSobreMesa[0] = imgDado1;
    	dadosSobreMesa[1] = imgDado2;
    	dadosSobreMesa[2] = imgDado3;
    	dadosSobreMesa[3] = imgDado4;
    	dadosSobreMesa[4] = imgDado5;
    	dadosRival[0] = dadoRival1;
    	dadosRival[1] = dadoRival2;
    	dadosRival[2] = dadoRival3;
    	dadosRival[3] = dadoRival4;
    	dadosRival[4] = dadoRival5;
    	tusDados[0] = dadoTuyo1;
    	tusDados[1] = dadoTuyo2;
    	tusDados[2] = dadoTuyo3;
    	tusDados[3] = dadoTuyo4;
    	tusDados[4] = dadoTuyo5;
	}
	
	/**
	 * Estableix la quantitat maxima de diners que es poden apostar.
	 */
	
	@FXML
    void subirApuesta(ActionEvent event) 
    {
		int max;
    	int dineroApostado = Integer.parseInt(labelApuesta.getText());
    	if (jugador1.getDinero() > jugador2.getDinero())
    	{
    		max = jugador2.getDinero();
    	}
    	else if (jugador1.getDinero() < jugador2.getDinero())
    	{
    		max = jugador1.getDinero();
    	}
    	else
    	{
        	max = jugador1.getDinero();
    	}    
    	btnSubirApuesta.setDisable(false);
    	btnBajarApuesta.setDisable(false);
    	dineroApostado += 50;
	    labelApuesta.setText(Integer.toString(dineroApostado));
    	if (dineroApostado >= max)
    	{
    		btnSubirApuesta.setDisable(true);
    	}
    }
	
	/**
	 * Estableix la quantitat minima de diners que es poden apostar.
	 */

    @FXML
    void bajarApuesta(ActionEvent event) 
    {
    	int dineroApostado = Integer.parseInt(labelApuesta.getText());
    	if (dineroApostado > 100) 
    	{
    		btnSubirApuesta.setDisable(false);
    		btnBajarApuesta.setDisable(false);
    		dineroApostado -= 50;
	    	labelApuesta.setText(Integer.toString(dineroApostado));
    	}
    	if (dineroApostado == 100)
    	{
    		btnBajarApuesta.setDisable(true);
    	}
    }
    
	/**
	 * Confirma la quantitat de diners apostats i tanca la finestra d'apostes.
	 */
    
    @FXML
    void confirmarApuesta(ActionEvent event) 
    {   
    	jugador1.setDineroApostado(Integer.parseInt(labelApuesta.getText()));
    	jugador2.setDineroApostado(Integer.parseInt(labelApuesta.getText()));
    	jugador1.setDinero(jugador1.getDinero() - Integer.parseInt(labelApuesta.getText()));
    	jugador2.setDinero(jugador2.getDinero() - Integer.parseInt(labelApuesta.getText()));
    	labelDinero.setText(Integer.toString(jugador1.getDinero()));
    	labelDineroRival.setText(Integer.toString(jugador2.getDinero()));    	
    	panelApuesta.setVisible(false);
    	panelOpaco.setVisible(false);
    	imgRondaGan.setVisible(false);
    }
    
	/**
	 * Crida al metode de tirar els daus i s'els estabelix al jugador 1.
	 */
    
    @FXML
    void tirarJugador1(ActionEvent event)
    {
    	int i;
    	for (i = 0; i < 5; i++)
    	{
    		tusDados[i].setVisible(false);
    	}
    	tirarDados(1);
    	btnTirarJugador1.setVisible(false);
    	btnRecogerDadosJ1.setVisible(true);
    }
    
    /**
	 * Posa les imatges corresponents a la banda del tauler del jugador 1.
	 */
    
    @FXML
    void recogerDadosJ1(ActionEvent event)
    {
    	int i;
    	for (i = 0; i < 5; i++)
    	{
    		tusDados[i].setImage(dadosSobreMesa[i].getImage());
    		tusDados[i].setVisible(true);
    		dadosSobreMesa[i].setVisible(false);
    	}
    	btnRecogerDadosJ1.setVisible(false);
    	btnFinalizarJ1.setVisible(true);
    }
    
    /**
	 * Passa el torn al jugador 2.
	 */
    
    @FXML
    void finalizarJ1(ActionEvent event)
    {
    	btnFinalizarJ1.setVisible(false);
    	btnTirarJugador2.setVisible(true);
    }
    
	/**
	 * Crida al metode de tirar els daus i s'els estabelix al jugador 2.
	 */
    
    @FXML
    void tirarJugador2(ActionEvent event)
    {
    	int i;
    	for (i = 0; i < 5; i++)
    	{
    		dadosRival[i].setVisible(false);
    	}
    	tirarDados(2);
    	btnTirarJugador2.setVisible(false);
    	btnRecogerDadosJ2.setVisible(true);
    }
    
    /**
	 * Posa les imatges corresponents a la banda del tauler del jugador 2.
	 */
    
    @FXML
    void recogerDadosJ2(ActionEvent event)
    {
    	int i;
    	for (i = 0; i < 5; i++)
    	{
    		dadosRival[i].setImage(dadosSobreMesa[i].getImage());
    		dadosRival[i].setVisible(true);
    		dadosSobreMesa[i].setVisible(false);
    	}
    	btnRecogerDadosJ2.setVisible(false);
    	btnFinalizarJ2.setVisible(true);
    }
    
    /**
	 * Crida als metodes que evaluen les tirades de cada jugador.
	 */
    
    @FXML
    void finalizarJ2(ActionEvent event)
    {
    	int j1;
    	int j2;
    	btnFinalizarJ2.setVisible(false);
    	j1 = evaluarJugador(valorDadosJ1);
    	j2 = evaluarJugador(valorDadosJ2);
    	evaluarGanador(j1, j2);
    	btnTirarJugador1.setVisible(true);
    }
    
    /**
	 * Suma els daus de cada jugador.
	 * @return Suma dels daus dels jugadors.
	 */
    
	private int evaluarJugador(int[] dadosJugador) 
	{		
		int valor = 0;
		
		valor = IntStream.of(dadosJugador).sum();
		System.out.println(valor);
		return valor;
	}
	
    /**
	 * Comprova quin dels dos jugadors es el guanyador.
	 */
	
	private void evaluarGanador(int valorJ1, int valorJ2) 
	{
		if (valorJ1 > valorJ2)
		{
			jugador1.setDinero(jugador1.getDineroApostado()* 2 + jugador1.getDinero());
			imgRondaGan.setImage(new Image("assets/ronda_j1.png"));
			imgRondaGan.setVisible(true);
		}
		else if (valorJ2 > valorJ1)
		{
			jugador2.setDinero(jugador2.getDineroApostado()* 2 + jugador2.getDinero());
			imgRondaGan.setImage(new Image("assets/ronda_j2.png"));
			imgRondaGan.setVisible(true);
		}
		else
		{
			jugador1.setDinero(jugador1.getDineroApostado() + jugador1.getDinero());
			jugador2.setDinero(jugador2.getDineroApostado() + jugador2.getDinero());
			imgRondaGan.setImage(new Image("assets/empate.png"));
			imgRondaGan.setVisible(true);
		}
		labelDinero.setText(Integer.toString(jugador1.getDinero()));
		labelDineroRival.setText(Integer.toString(jugador2.getDinero()));
		if (jugador1.getDinero() == 0)
		{
			System.out.println("Fin");
			panelOpaco.setVisible(true);
			imgGanador.setImage(new Image("assets/gana_j2.jpg"));
			panelGanador.setVisible(true);
		}
		else if (jugador2.getDinero() == 0)
		{
			System.out.println("Fin");
			panelOpaco.setVisible(true);
			imgGanador.setImage(new Image("assets/gana_j1.jpg"));
			panelGanador.setVisible(true);
		}
		else
		{
			panelOpaco.setVisible(true);
			panelApuesta.setVisible(true);
		}
	}
	
    /**
	 * Tanca el programa
	 */
	
	 @FXML
	 void salir(ActionEvent event)
	 {
		 System.exit(0);
	 }
	 
	/**
	* Torna a possar els diners inicials a cada jugador per a una altra partida.
	*/
	 
	 @FXML
	 void repetir(ActionEvent event)
	 {
		 jugador1.setDinero(dineroInicial);
		 jugador2.setDinero(dineroInicial);
		 panelGanador.setVisible(false);
		 panelApuesta.setVisible(true);
	 }
	 
	 
	/**
	* Genera aleatoriament la tirada de cada jugador.
	*/
	private void tirarDados(int jugador) 
	{
		int i;
		int valor;
	    for (i = 0; i < 5; i++) 
	    {
	    	valor = random.nextInt(6) + 1;
	    	switch(valor) 
	    	{
	    		case 1:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado1.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 1;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 1;
	    			}
	    			break;
	    		case 2:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado2.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 2;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 2;
	    			}
	    			break;
	    		case 3:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado3.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 3;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 3;
	    			}
	    			break;
	    		case 4:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado4.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 4;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 4;
	    			}
	    			break;
	    		case 5:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado5.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 5;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 5;
	    			}
	    			break;
	    		case 6:
	    			dadosSobreMesa[i].setImage(new Image("assets/dado6.png"));
	    			dadosSobreMesa[i].setVisible(true);
	    			if (jugador == 1)
	    			{
	    				valorDadosJ1[i] = 6;
	    			}
	    			else
	    			{
	    				valorDadosJ2[i] = 6;
	    			}
	    			break;   			
	    	}  	
    	}
	}    
}
