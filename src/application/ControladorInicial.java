package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * <h2>Controlador principal de la aplicacio</h2>
 * 
 * Creacio de la finestra del joc i especificacio de paramtres dels jugadors.
 * @author Jonathan Raya Rios
 * @since 10-03-19
 */

public class ControladorInicial implements Initializable{
	
	static int dineroJugador1 = 1000;
	@FXML
	private Button btnBajarDinero;
	
	@FXML
	private Button btnSubirDinero;
	
    @FXML
    private Label labelDinero;
	
    @FXML
    private Button btnIniciar;
	
	/**
	 * Metode que crea la finestra del joc.
	 * @see ControladorJuego
	 */
    
    @FXML
    void iniciar(ActionEvent event) throws InterruptedException 
    {
    	
    	try {
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/GraficosJuego.fxml"));	
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.getIcons().add(new Image(ControladorInicial.class.getResourceAsStream("/assets/dado1.png")));
			stage.setTitle("Póquer de Dados");
			scene.getStylesheets().add(getClass().getResource("Estilo.css").toExternalForm());
			stage.setScene(scene);
			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
    	((Node) event.getSource()).getScene().getWindow().hide();	
    }

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) 
	{
		
	}
	
	/**
	 * Metode que estableix la quantitat maxima de diners inicials.
	 */
	
    @FXML
    void subirDinero(ActionEvent event) 
    {
    	int dinero = Integer.parseInt(labelDinero.getText());
    	if (dinero < 2000) 
    	{
    		btnBajarDinero.setDisable(false);
    		btnSubirDinero.setDisable(false);
    		dinero += 50;
	    	labelDinero.setText(Integer.toString(dinero));
    	}
    	if (dinero == 2000)
    	{
    		btnSubirDinero.setDisable(true);
    	}
    	dineroJugador1 = dinero;
    }
    
	/**
	 * Metode que estableix la quantitat minima de diners inicials.
	 */
    
    @FXML
    void bajarDinero(ActionEvent event) 
    {
    	int dinero = Integer.parseInt(labelDinero.getText());
    	if (dinero > 500) 
    	{
    		btnSubirDinero.setDisable(false);
    		btnBajarDinero.setDisable(false);
	    	dinero -= 50;
	    	labelDinero.setText(Integer.toString(dinero));
    	}
    	if (dinero == 500)
    	{
    		btnBajarDinero.setDisable(true);
    	}
    	dineroJugador1 = dinero;
    }
}
