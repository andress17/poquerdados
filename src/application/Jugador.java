package application;

/**
 * <h2>Dades dels jugadors</h2>
 * 
 * Creacio de les dades personals de cada jugador.
 * @author Jonathan Raya Rios
 * @since 10-03-19
 */

public class Jugador {
	
	private int dinero = 1000;
	private int dineroApostado = 0;
	int[] dados = new int[5];
	
	/**
	 * Constructor de la classe Jugador
	 * @param dinero Rebeix la quantitat de diners establertes en ñla classe ControladorInicial.
	 * @see ControladorInicial
	 */
	
	public Jugador (int dinero)
	{		
		this.dinero = dinero;
		System.out.println("Creado");
	}
	
	/**
	 * Metode per modificar la quantitat de diners de cada jugador al llarg de la partida.
	 */
	
	public void setDinero (int dinero)
	{
		System.out.println(dinero);
		this.dinero = dinero;
	}
	
	/**
	 * Metode que retorna la quantitat de diners actuals de cada jugador.
	 * @return Diners del jugador.
	 */
	
	public int getDinero ()
	{
		return dinero;
	}
	
	/**
	 * Metode que guarda els diners que s'han apostat en cada ronda.
	 */
	
	public void setDineroApostado (int dinero)
	{
		dineroApostado = dinero;
	}
	
	/**
	 * Metode que retorna els diners que s'han apostat en cada ronda.
	 * @return Diners que ha apostat el jugador.
	 */
	
	public int getDineroApostado ()
	{
		return dineroApostado;
	}
}
