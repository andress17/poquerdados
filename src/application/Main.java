package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;

/**
 * <h2>Main</h2>
 * 
 * Clase principal del programa.
 * @author Jonathan Raya Rios
 * @since 10-03-19
 */

public class Main extends Application {
	
	/**
	 * Metode que crea la finestra principal de la aplicacio.
	 * @see ControladorInicial
	 */
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/PantallaInicial.fxml"));	
			Scene scene = new Scene(root);
			primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/assets/dado1.png")));
			primaryStage.setTitle("Póquer de Dados");

			scene.getStylesheets().add(getClass().getResource("Estilo.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		launch(args);
	}
}
